Some config files.

For my nvim config to work you need [vim-plug](https://github.com/junegunn/vim-plug)

Preview of what my i3, polybar, alacritty and nvim configs look like:
![preview](preview.png)
